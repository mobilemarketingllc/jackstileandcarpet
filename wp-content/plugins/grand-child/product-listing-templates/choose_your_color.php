<?php
/*
Template Name: Coolorwall Color Page 
*/

//Coolorwall Style Page Landing

$post_types = 'luxury_vinyl_tile';
$style_page_url = "/choose-your-style/"; 
function unique_multidim_array($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    return $temp_array; 
}

get_header();
?>
<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
            <?php
                
                $args = get_posts(array(
                    'numberposts'	=> -1,
                    'post_type'		=> $post_types,
                    'meta_query'	=> array(
                        array(
                            'key'	 	=> 'collection',
                            'value'	  	=> 'COREtec Colorwall',
                            'compare' 	=> '=',
                        ),
                    ),
                ));

                $the_query = new WP_Query( $args );
                $colornames = array();
                foreach( $the_query->query as $color ) {
                    $productid = $color->ID;
                    $colorname = array(
                        "ID" => $productid,
                        "Color" => get_field('color', $productid),
                    );
                    array_push($colornames, $colorname);
                }

                $finalcolornames = unique_multidim_array($colornames,'Color');
                if(!empty($finalcolornames)){
            ?>

            <div class="colors-list">
                <div class="top-heading">
                    <h5>COREtec Colorwall</h5>
                    <h3>
                        <span class="first-heading-text">1.</span>
                        <span class="second-heading-text">Choose your Color</span>
                    </h3>
                </div>
                <ul>
                <?php
                    foreach( $finalcolornames as $colors ) { 
                    if(get_field('swatch_image_link', $colors["ID"])) {
                ?>
                    <li>
                        <div class="fl-post-grid-image prod_img">
                            <a href="<?php echo $style_page_url.'?color='.$colors["Color"]; ?>" title="<?php the_title_attribute(); ?>">
                                <?php
                                    $itemImage = get_field('swatch_image_link', $colors["ID"]);
                                                                    
                                    // if(strpos($itemImage , 's7.shawimg.com') !== false){
                                    //     if(strpos($itemImage , 'http') === false){ 
                                    //         $itemImage = "http://" . $itemImage;
                                    //     }
                                    //     $class = "";
                                    // }else{
                                    //     if(strpos($itemImage , 'http') === false){ 
                                    //         $itemImage = "https://" . $itemImage;
                                    //     }
                                    //     $class = "shadow";
                                    // }							  
                                    // $image= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage . "]&scale=size[300x300]&sink";
                                    
                                ?>
                                <img class="<?php echo $class; ?>" src="<?php echo $itemImage; ?>" alt="<?php the_title_attribute(); ?>" />
                            </a>
                            <div class="product-info-box">
                                <div class="product-info-box-inner">
                                    <h6><?php echo $colors["Color"]; ?></h6>
                                    <a href="<?php echo $style_page_url.'?color='.$colors["Color"]; ?>" class="readmore-link">Choose Your Style</a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php  }
                } ?>
                </ul>
            </div>
            <?php } ?>
            <?php wp_reset_query(); ?>
        </div>
	</div>
</div>

<?php get_footer(); ?>