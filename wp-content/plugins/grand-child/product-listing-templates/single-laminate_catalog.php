<?php include( dirname( __FILE__ ) . '../include/constant.php' );?>
<?php get_header(); ?>
<div class="container">
    <div class="row">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/" style="padding-top: 20px; padding-left: 20px;">
       <a href="/">Home</a> &raquo; <a href="/flooring/">Flooring</a> &raquo; <a href="/flooring/laminate/">Laminate</a> &raquo; <a href="/flooring/laminate/products/">Laminate Products</a> &raquo; <?php the_title(); ?>
    </div>
        </div>
</div>

<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12<?php //FLTheme::content_class(); ?>">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php 
				if($LAYOUT_COL == 0)
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product.php';
				else
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-'.$LAYOUT_COL.'.php';

				include( $dir ); 
			?>
				<?php //get_template_part('content', 'single-product'); ?>
			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>
	</div>
</div>
<?php get_footer(); ?>